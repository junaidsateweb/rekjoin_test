import React, { Component } from "react";
import {
  TouchableOpacity,
  Platform,
  StyleSheet,
  Button,
  Image,
  View,
  Text
} from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator,
  TabBarBottom,
  createSwitchNavigator,
  createMaterialTopTabNavigator,
  TabBarTop,
  withNavigation
} from "react-navigation";
import LoginCheckScreen from "@containers/LoginCheck";
import LoginScreen from "@containers/LoginScreen";
import SignupScreen from "@containers/SignupScreen";
import NotificationScreen from "@containers/NotificationScreen";
import SessionEditScreen from "@containers/SessionEditScreen";
import SessionNewScreen from "@containers/SessionNewScreen";
import AllSessionListScreen from "@containers/AllSessionListScreen";
import MySessionListScreen from "@containers/MySessionListScreen";
import JoinSessionScreen from "@containers/JoinSessionScreen";
import SesionMembersScreen from "@containers/SesionMembersScreen";
import InvitePlayerScreen from "@containers/InvitePlayerScreen";
import ChatHomeScreen from "@containers/ChatHomeScreen";
import ChatScreen from "@containers/ChatScreen";
import UserProfileScreen from "@containers/UserProfileScreen";
import EndSession from "@containers/EndSession";
import FollowersList from "@containers/FollowersList";
import ProfileEditScreen from "@containers/ProfileEditScreen";
import Profile from "@containers/Profile";
import ForgotPassword from "@containers/ForgotPassword";
import ChangePassword from "@containers/ChangePassword";
import SesionReportScreen from "@containers/Reported";
import { FullAds } from "@components/Ads";
import NavigationService from "./src/routes/NavigationService";
import Icon from "@components/Icon";
import Colors from "@theme/Colors";
import Styles from "@theme/Styles";

// const { navigate } = this.props.navigation;
const SessionTabNavigator = createMaterialTopTabNavigator(
  {
    AllSessionList: {
      screen: AllSessionListScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor, focused }) => (
          <Text
            style={{
              color: tintColor,
              fontSize: 12,
              fontFamily: Platform.OS == "ios" ? "Krub" : "Krub SemiBold",
              fontWeight: Platform.OS == "ios" ? "bold" : "normal",
              fontStyle: Platform.OS == "ios" ? "normal" : "normal"
            }}
          >
            All Session List
          </Text>
        )
      }
    },
    MySessionList: {
      screen: MySessionListScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor, focused }) => (
          <Text
            style={{
              color: tintColor,
              fontSize: 12,
              fontFamily: Platform.OS == "ios" ? "Krub" : "Krub SemiBold",
              fontWeight: Platform.OS == "ios" ? "bold" : "normal",
              fontStyle: Platform.OS == "ios" ? "normal" : "normal"
            }}
          >
            My Session List
          </Text>
        )
      }
    }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: "top",

    tabBarOptions: {
      style: {
        elevation: 0,
        shadowColor: "transparent",
        backgroundColor: "#E8E8E8"
      },
      showIcon: false,
      tabStyle: {
        height: 60,
        paddingVertical: 8
      },
      iconStyle: {
        flexGrow: 0,
        marginBottom: 0,
        marginTop: 0
      },
      labelStyle: {
        justifyContent: "center",
        alignItems: "center",
        fontSize: 12
      },
      indicatorStyle: { height: null, top: 0, backgroundColor: "#E8E8E8" },
      activeTintColor: "#cc0000",
      inactiveTintColor: "#808080"
    },
    lazy: true,
    animationEnabled: false,  
    swipeEnabled: false
  },
  {
    // initialRouteName:  'MySessionList',
  }
);

const sessionNavigator = createStackNavigator(
  {
    SessionHome: {
      screen: SessionTabNavigator,

      navigationOptions: prop => ({
        title: "Session",
        headerStyle: {
          backgroundColor: "#cc0000"
        },
        headerRight: (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => prop.navigation.navigate("Notification")}
            style={{
              marginHorizontal: 14,
              marginRight: Platform.OS == "ios" ? 24 : 16
            }}
          >
            <Icon
              family="Ionicons"
              name="ios-notifications"
              size={25}
              color="#FFFFFF"
            />
          </TouchableOpacity>
        ),
        headerLeft: <View style={{ marginHorizontal: 15 }} />,

        headerTintColor: "#FFFFFF",
        headerTitleStyle: {
          fontSize: 25,
          fontFamily: Platform.OS == "ios" ? "Sansation" : "Sansation Bold",
          fontWeight: Platform.OS == "ios" ? "bold" : "normal",
          flex: 1,
          alignSelf: "center",
          textAlign: "center",
          backgroundColor: "transparent"
        }
      })
    },

    SessionEdit: {
      screen: SessionEditScreen,

      navigationOptions: {
        headerVisible: false
      },
      headerMode: "none"
    },

    SessionNew: {
      screen: SessionNewScreen,

      navigationOptions: {
        headerVisible: false
      },
      headerMode: "none"
    },
    InvitePlayer: {
      screen: InvitePlayerScreen,
      // navigationOptions: {
      //   headerVisible: false
      // },
      headerMode: "none"
    },
    UserProfile: {
      screen: UserProfileScreen,
      navigationOptions: {
        headerVisible: false
      },
      headerMode: "none"
    },

    SessionJoin: {
      screen: JoinSessionScreen,
      navigationOptions: {
        headerVisible: false
      },
      headerMode: "none"
    },
    SesionMembers: {
      screen: SesionMembersScreen,
      navigationOptions: {
        headerVisible: false
      },
      headerMode: "none"
    },
    SesionReport: {
      screen: SesionReportScreen,
      navigationOptions: {
        headerVisible: false
      }
    },
    ProfileView: {
      screen: Profile
    }
  },
  {
    // initialRouteName: "InvitePlayer"
  }
);

sessionNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};
const chatNavigator = createStackNavigator(
  {
    ChatHome: {
      screen: ChatHomeScreen
    },

    ChatIn: {
      screen: ChatScreen
    }
  },
  {
    // initialRouteName:  'Account',
  }
);
chatNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const profileNavigator = createStackNavigator(
  {
    Profile: {
      screen: Profile
    },
    FollowersList: {
      screen: FollowersList
    },
    UserProfile2: {
      screen: UserProfileScreen
    },
    ProfileEdit: {
      screen: ProfileEditScreen
    },
    ChangePassword: {
      screen: ChangePassword
    }
  },
  {
    // initialRouteName:  'ProfileEdit',
  }
);

profileNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const Tabscreen = createMaterialTopTabNavigator(
  {
    Session: {
      screen: sessionNavigator,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          focused ? (
            <Icon
              name="slideshare"
              family="FontAwesome"
              size={24}
              color={tintColor}
            />
          ) : (
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Icon
                name="slideshare"
                family="FontAwesome"
                size={26}
                color={tintColor}
              />
            </View>
          ),
        tabBarLabel: ({ tintColor, focused }) =>
          focused ? (
            <Text
              style={{
                color: tintColor,
                fontSize: 10,
                fontFamily:
                  Platform.OS == "ios" ? "Krub" : "Krub Medium Italic",
                fontWeight: Platform.OS == "ios" ? "bold" : "normal",
                fontStyle: Platform.OS == "ios" ? "italic" : "normal"
              }}
            >
              Session
            </Text>
          ) : null
      }
    },
    Chat: {
      screen: chatNavigator,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          focused ? (
            <Icon name="chat" family="Entypo" size={24} color={tintColor} />
          ) : (
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Icon name="chat" family="Entypo" size={26} color={tintColor} />
            </View>
          ),
        tabBarLabel: ({ tintColor, focused }) =>
          focused ? (
            <Text
              style={{
                color: tintColor,
                fontSize: 10,
                fontFamily:
                  Platform.OS == "ios" ? "Krub" : "Krub Medium Italic",
                fontWeight: Platform.OS == "ios" ? "bold" : "normal",
                fontStyle: Platform.OS == "ios" ? "italic" : "normal"
              }}
            >
              Chat
            </Text>
          ) : null
      }
    },
    Profile: {
      screen: profileNavigator,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          focused ? (
            <Icon name="person" size={24} color={tintColor} />
          ) : (
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Icon name="person" size={26} color={tintColor} />
            </View>
          ),
        tabBarLabel: ({ tintColor, focused }) =>
          focused ? (
            <Text
              style={{
                color: tintColor,
                fontSize: 10,
                fontFamily:
                  Platform.OS == "ios" ? "Krub" : "Krub Medium Italic",
                fontWeight: Platform.OS == "ios" ? "bold" : "normal",
                fontStyle: Platform.OS == "ios" ? "italic" : "normal"
              }}
            >
              Profile
            </Text>
          ) : null
      }
    }
  },
  {
    lazy: true,
    initialRouteName: "Session",
    tabBarComponent: TabBarTop,
    tabBarPosition: "bottom",
    tabBarOptions: {
      style: {
        backgroundColor: "#808080"
      },
      upperCaseLabel: false,
      labelStyle: {
        margin: 0
      },
      showLabel: true,
      showIcon: true,
      tabStyle: {
        height: 60
      },
      iconStyle: {
        flexGrow: 0,
        marginBottom: 5
      },
      indicatorStyle: {
        backgroundColor: "#cc0000",
        height: null,
        top: 0
      },
      activeTintColor: "#FFFFFF",
      inactiveTintColor: "#D9D9D9"
    },
    animationEnabled: true,
    swipeEnabled: false
  }
);
const NotificationRout = createSwitchNavigator(
  {
    Notification: {
      screen: NotificationScreen
    },
    EndSession: {
      screen: EndSession
    }
  },
  {
    initialRouteName: "Notification"
  }
);
const Tabscreen_n = createSwitchNavigator(
  {
    Tabscreen: {
      screen: Tabscreen
    },
    Notification: {
      screen: NotificationRout
    }
  },
  {
    initialRouteName: "Tabscreen",
    navigationOptions: {
      headerVisible: true
    }
  }
);

const AppNavigator = createSwitchNavigator(
  {
    LoginCheck: {
      screen: LoginCheckScreen
    },

    Signup: {
      screen: SignupScreen
    },

    Login: {
      screen: LoginScreen
    },
    ForgotPassword: {
      screen: ForgotPassword
    },
    Tabscreen_n: {
      screen: Tabscreen_n
    }
  },
  {
    // initialRouteName: "Notification",
    initialRouteName: "LoginCheck",
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
       
    );
  }
}
