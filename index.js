/** @format */

import { AppRegistry } from "react-native";
import bgMessaging from "./src/bgMessaging"; // <-- Import the file you created
import App from "./App";
import MySessionListScreen from "@containers/MySessionListScreen";

import { name as appName } from "./app.json";

AppRegistry.registerComponent(appName, () => App);
