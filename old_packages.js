import AsyncStorage from "@react-native-community/async-storage";
import RNFS from "react-native-fs";
import MaskedView from "@react-native-community/masked-view";
import NetInfo from "@react-native-community/netinfo";
import Slider from "@react-native-community/slider";
import ViewPager from "@react-native-community/viewpager";

export { AsyncStorage, NetInfo, Slider, ViewPager, MaskedView, RNFS };

// "@react-native-community/async-storage": "^1.5.0",
//     "@react-native-community/masked-view": "^0.1.1",
//     "@react-native-community/netinfo": "^3.2.1",
//     "@react-native-community/slider": "^1.1.4",
//     "@react-native-community/viewpager": "^1.1.7",

// yarn add @react-native-community/viewpager
// react-native link @react-native-community/viewpager
// yarn add @react-native-community/slider
// react-native link @react-native-community/slider

// $ yarn add @react-native-community/async-storage
// $ react-native link @react-native-community/async-storage

// yarn add @react-native-community/netinfo
// react-native link @react-native-community/netinfo

// yarn add react-native-fs
// react-native link react-native-fs

// $ yarn add @react-native-community/masked-view
// $ react-native link @react-native-community/masked-view


